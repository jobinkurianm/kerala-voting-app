import { LOAD_VOTE_FAILURE, REQUEST_VOTS, 
  RECEIVED_VOTES,CAST_VOTE_REQUEST,
  CAST_VOTE_SUCCESSFUL,CAST_VOTE_FAILED,SHOW_VOTE_RESULT } from '../actions/voteActions';

const initialState = {
  votes: {},
  isLoading: false,
  isError: false,
  isShowResult:false
};

const voteReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_VOTS:
      return Object.assign({}, state, { isLoading: true })
    case RECEIVED_VOTES:
      return Object.assign({}, state, { votes: action.payload, isLoading: false })
    case LOAD_VOTE_FAILURE:
      return Object.assign({}, state, { isError: true, isLoading: false })
      case SHOW_VOTE_RESULT:
        return Object.assign({}, state, { isShowResult: true })
      case CAST_VOTE_REQUEST:
        return Object.assign({}, state, { isLoading: true })
      case CAST_VOTE_SUCCESSFUL:
        return Object.assign({}, state, { votes: action.payload, isLoading: false })
      case CAST_VOTE_FAILED:
        return Object.assign({}, state, { isError: true, isLoading: false })
    default:
      return state;
  }
};

export default voteReducer;