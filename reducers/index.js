import { combineReducers } from 'redux';

import voteReducer from './voteReducer';
const rootReducer = combineReducers({
  voteState: voteReducer
});

export default rootReducer;