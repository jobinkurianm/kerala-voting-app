/*
Just like componentWillReceiveProps, getDerivedStateFromProps is invoked whenever a component receives new props.

getDerivedStateFromProps is invoked right before calling the render method,
both on the initial mount and on subsequent updates. It should return an object to update the state, or null to update nothing.

class List extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if (props.selected !== state.selected) {
      return {
        selected: props.selected,
      };
    }

    // Return null if the state hasn't changed
    return null;
  }

  // ...
}
*/


/*
FILTERRING OBJECT PROPERTIES BASED ON VALUE*****

Here are two vanilla javascript options:

A.: Iterate over the object's keys and delete those having a falsey value.

var obj = {
  propA: true,
  propB: true,
  propC: false,
  propD: true,
};

Object.keys(obj).forEach(key => {
  if (!obj[key]) delete obj[key];
});

console.log(obj);
*/



/*
FUNCTION INSIDE RENDER
https://stackoverflow.com/questions/42645297/function-inside-render-and-class-in-reactjs
render(){
  
  // An array of user objects & a status string.
  const { users, status } = this.props;
  
  // Map users array to render your children:
  const renderUserList = () => {
    return users.map(user => {
      return <div>{ user.firstName }</div>;
    });
  };
  
  // Conditionally load a component:
  const renderStatus = () => {
    let component = '';
    switch(status){
      case 'loading':
        component = <Component1 />
        break;
      case 'error':
        component = <Component2 />
        break;
      case 'success':
        component = <Component3 />
        break;
      default:
        break;
    }
    
    return component;
  }
  
  // render() return:
  return(
    <div>
      <div className="status">
        { renderStatus() }
      </div>
      <div className="user-list">
        { renderUserList() }
      </div>
    </div>
  );
}
*/

/*
*CONTTRUCTOR AND BINDING IN REACT NATIVE

1.It used for initializing the local state of the component by assigning an object to this.state.
2.It used for binding event handler methods that occur in your component.
https://www.freecodecamp.org/news/this-is-why-we-need-to-bind-event-handlers-in-class-components-in-react-f7ea1a6f93eb/

import React, { Component } from 'react';   
  
class App extends Component {  
  constructor(props){  
    super(props);  
    this.state = {  
         data: 'www.javatpoint.com'  
      }  
    this.handleEvent = this.handleEvent.bind(this);  
  }  
  handleEvent(){  
    console.log(this.props);  // 'this' is undefined if we didnt excicute bind function like (this.handleEvent.bind(this))
  }  
  render() {  
    return (  
      <div className="App">  
    <h2>React Constructor Example</h2>  
    <input type ="text" value={this.state.data} />  
        <button onClick={this.handleEvent}>Please Click</button>  
      </div>  
    );  
  }  
}  
export default App;   
*/

/*
*CONTTRUCTOR IN REACT NATIVE (tick stack overflow)
https://stackoverflow.com/questions/45456011/react-native-how-to-use-state-in-constructor
constructor(props){
 super(props);
  this.state = {
     check: false
   } 
} 

render() {
 <View>
 {this.props.services && this.props.services.map(
  <CheckBox
            center
            title={d}
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked= {true}
            onPress={() => this.checkBoxClick()}
        />
  )</View>}
}
*/


/*

*CONTTRUCTOR IN REACT NATIVE

class CounterButton extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {count: 1};
  }

  render() {
    return (
      <View>
        <Button
          title="Press me"
          color={this.props.color}
          onPress={() => this.setState(state => ({count: state.count + 1}))}
        />
        <Text>
          Count: {this.state.count}
        </Text>
      </View>
    );
  }
}
*/


/*
CONDITIONAL RENDERING 
class AdminToolbar extends React.Component
{
    render()
    {
        if(!this.props.isAdmin)
        {
            return null;
        }
        return (
            <div>
                {  }
                </div>
                );
            }
        }
*/


/*
CONDITIONAL RENDERING 
let adminBar = null;
if(this.props.isAdmin)
{
    adminBar = <AdminToolbar/>;
}
return (
    { adminBar }
    <PrimaryContent/>
    <Sidebar/>
);
*/


/*
CONDITIONAL RENDERING 

class ConditionWith1Branch extends Component {
  state = {
    user: { name: "Fred", isAdmin: true }
  };
  render() {
    return this.state.user.isAdmin && <Button>Reset Data</Button>;
  }
}
*/


/*
CONDITIONAL RENDERING 

render() {
  const isLoggedIn = this.state.isLoggedIn;
  return (
    <div>
      The user is <b>{isLoggedIn ? 'currently' : 'not'}</b> logged in.    </div>
  );
}
*/


/*
CONDITIONAL RENDERING -1

class LoginControl extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.state = {isLoggedIn: false};
  }

  handleLoginClick() {
    this.setState({isLoggedIn: true});
  }

  handleLogoutClick() {
    this.setState({isLoggedIn: false});
  }

  render() {
    const isLoggedIn = this.state.isLoggedIn;
    let button;
    if (isLoggedIn) {
      button = <LogoutButton onClick={this.handleLogoutClick} />;
    } else {
      button = <LoginButton onClick={this.handleLoginClick} />;
    }

    return (
      <div>
        <Greeting isLoggedIn={isLoggedIn} />
        {button}
      </div>
    );
  }
}

ReactDOM.render(
  <LoginControl />,
  document.getElementById('root')
);
*/

/*
CONDITIONAL RENDERING -1


function WithLoading(Component) {
  return function WithLoadingComponent({ isLoading, ...props }) {
    return (isLoading ? <p>Loading ... please wait ...</p> : <Component {...props} />)
  }
}

const MyButton = (props) => {
  return <Button>My Button</Button>
}

const MyButtonWithLoading = WithLoading(MyButton);

class App extends Component {
  state = {isLoading: true}
  render() {
    return (
        <MyButtonWithLoading isLoading={this.state.isLoading} />
      </div>
    );
  }
}
Conclusion
*/


/*
mapStateToProps IN FUNCTONAL COMPONENT

import React from 'react'
import { connect } from 'react-redux'
import { fetchPosts } from '../actions'

let Button = ({ getPosts, channel }) => (
  <button
    onClick={() => { getPosts(channel) }}
    className="btn btn-primary btn-lg btn-block" >
    Get top news
  </button>
);

const mapStateToProps = (state) => ({
  channel: state.channel
})

const mapDispatchToProps = {
  getPosts: fetchPosts
}

Button = connect(
  mapStateToProps,
  mapDispatchToProps
)(Button)

export default Button;

*/



/*

CONDITIONAL RENDERING -1

{dataEntries.length && (
  <View>
    <Text>Visible only when array is not empty</Text>
  </View>
)}
*/

/*

CONDITIONAL RENDERING -2

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};
    
    this.handleClick = this.handleClick.bind(this);
  }
  
  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }
  
  render() {
    return (
      <div>
        <Header />
        { this.state.isToggleOn && <Subheader /> }
        <Content />
        <button onClick={this.handleClick}>
          { this.state.isToggleOn ? 'ON' : 'OFF' }
        </button>
      </div>
    );
  }
}

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
*/


/*
CONDITIONAL RENDERING -3
if (isVillain) {
    return (
      <View>
        <Text style={styles.deepVoice}>'General React'oni!'</Text>
      </View>
    )
  }

  return (
    <View>
      <Text>'Hello there!'</Text>
    </View>
  )
*/




/*
FUNCTIONAL COMPONENT


import React, { useState} from 'react';
import {Button, View, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import LoginForm from './Login';

const ModalDemo = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };
  return (
    <View style={styles.container}>
      <Button title="Click here to login" onPress={toggleModal} />
      <Modal
        isVisible={isModalVisible}>
        <View>
          <LoginForm />
          <View>
            <Button title="Hide modal" onPress={toggleModal} />
          </View>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E6E6FA',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default ModalDemo;*/