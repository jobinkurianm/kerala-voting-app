import { Platform } from 'react-native';
import * as constants from './constants';

let API_URL = __DEV__ ? constants.API_STG : constants.API_PRD;

export default {
  API_URL,
  ANALYTICS_KEY: Platform.select({
    ios: constants.IOS_ANALYTICS_KEY,
    android: constants.ANDROID_ANALYTICS_KEY,
  }),
};