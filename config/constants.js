
// APIS
export const API_PRD = "https://firbase.prd/v2";
export const API_STG = "https://firbase.stg/v2";
export const IOS_ANALYTICS_KEY = "IOS_KEY";
export const ANDROID_ANALYTICS_KEY = "ANDROID_KEY";

