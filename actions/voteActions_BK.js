import { db } from '../config/db.config';
export const LOAD_VOTE_FAILURE = 'LOAD_VOTE_FAILURE';
export const REQUEST_VOTS = 'REQUEST_VOTS';
export const RECEIVED_VOTES = 'RECEIVED_VOTES';
export const CAST_VOTE_REQUEST = 'CAST_VOTE_REQUEST';
export const CAST_VOTE_SUCCESSFUL = 'CAST_VOTE_SUCCESSFUL';
export const CAST_VOTE_FAILED = 'CAST_VOTE_FAILED';


export const loadVote = () => {

    return (dispatch, getState) => {
        dispatch(requestVote());
        db.ref("polldata/").once("value").then((snapshot) => {
            var votes = snapshot.val();
            dispatch(receivedVotes(votes));
            }).catch((error) =>{
                dispatch(requestVoteError(error));
            });
    }
    /*
    dispatch(requestVote());
    db.ref("polldata/").on("value", function(snapshot) {
    var votes = snapshot.val();
    dispatch(receivedVotes(votes));
    //dispatch({ type: 'LOAD_QUOTE_SUCCESS', payload: votes })
    }, function (error) {
    //dispatch({ type: 'LOAD_QUOTE_FAILURE', payload: error })
    dispatch(requestVoteError(error));
    });
    */
}

export const castVote = (newVote) => {

    let voteTo = newVote.id;
    var voteEntry = Object.assign({}, newVote);
    
    return (dispatch, getState) => {
        dispatch({ type: CAST_VOTE_REQUEST});
        let currentState = getState().voteState;
        var newCount = currentState.votes[voteTo] ? currentState.votes[voteTo].vote+1 : 1;
        voteEntry.vote = newCount;
        var newPayload = Object.assign({}, currentState.votes,{[voteTo]:voteEntry});
        console.log(currentState.votes);
        console.log(voteEntry);
        console.log(newPayload);
        db.ref("polldata/"+voteTo).update(voteEntry).then(() => {
            console.log("INSERTED");
            dispatch({ type: CAST_VOTE_SUCCESSFUL,payload: newPayload});
            }).catch((error) =>{
            console.log(error);
            dispatch({ type: CAST_VOTE_FAILED});
            });
    }
}

  export const requestVote = () => ({
    type: REQUEST_VOTS
  });

  export const requestVoteError = data => ({
    type: LOAD_VOTE_FAILURE,
    payload: data,
  });
  
  export const receivedVotes = data => ({
    type: RECEIVED_VOTES,
    payload: data,
  });
  
  
