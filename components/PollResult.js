import React, { Component } from "react";
import config from '../config/web.config';
import { connect } from 'react-redux';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button
} from "react-native";
import { db } from '../config/db.config';
import {surveyList} from '../config/surveyList';
import ErrorPage from '../components/ErrorPage';
import Loading from '../components/Loading';


class PollResult extends Component {

    constructor(props){
        super(props);
         this.state = {
            totalVotes: 0,
            votes:[]
          } 
       } 

    static getDerivedStateFromProps(props,state) {

        var totalVotes = 0; 
        let votesToDisplay = [];
        var maxVoteObjKey = '';
        var biggestCount = 0;
        var totalPercentRounded = 0;
        //Filter votes to display : - Only display votes those are in the configuration array
         Object.keys(props.votes).forEach((key) => {
               surveyList.forEach(item => {
                   if(item.id === key){
                       var currVote = props.votes[key].vote;
                       //Finidng biggest vote element key
                       if(currVote > biggestCount) {
                           biggestCount = currVote;
                           maxVoteObjKey = key
                        }
                       totalVotes += currVote;
                       votesToDisplay.push(props.votes[key]);
                       return;
                   }
                 });
          });
          //Adding percentage to updated array
          votesToDisplay.forEach(obj => {
            var percentage = Math.floor((obj.vote/totalVotes)*100);
            obj['percentage'] = percentage;
            totalPercentRounded += percentage;
          });
          console.log("--Modified Array--");
          console.log(votesToDisplay)
          console.log("--Modified Array ends--");
          var adjustedPercent = 100 - totalPercentRounded;
          var arrIndexToAdjust = votesToDisplay.findIndex(x => x.id === maxVoteObjKey);
          //Adjusted the fraction percentage to biggest voted person
          votesToDisplay[arrIndexToAdjust].percentage  = votesToDisplay[arrIndexToAdjust].percentage + adjustedPercent;
          console.log("maxVoteKey-"+maxVoteObjKey+"   "+"arrIndexToAdjust-"+arrIndexToAdjust+"   "+"totalPercentRounded-"+totalPercentRounded+" adjustedPercent-"+adjustedPercent)
          console.log("--Fraction added Array--");
          console.log(votesToDisplay)
          console.log("--Fraction added Array ends--");
        return {
            votes: votesToDisplay,
            totalVotes:totalVotes
        };
    }

      votePercent(vote){  
        return(Math.floor((vote/this.state.totalVotes)*100));
      }  


    render() {

        console.log(this.state);

        if (this.props.isLoading) {
            return (
                <Loading/>
            )
          }
          if (this.props.isError) {
            return (
                <ErrorPage/>
            )
          }

        return (
            <View>
                <View>
                    {this.state.votes.map((key, index) => (
                    <Text key={index} style={{ fontSize: 20 }}>{key.name} : - {key.vote} - {key.percentage} %  ({this.votePercent(key.vote)})</Text>
                    ))}
                </View>
                <View>
                    <Text>Total Votes : {this.state.totalVotes}</Text>
                </View>
             </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log(state.voteState);
    // {Object.keys(this.props.votes).map((key) => (<Text key={key} style={{ fontSize: 20 }}>{this.props.votes[key].name} : - {this.props.votes[key].vote}</Text>))} 
	//return {user: state.users.find(user => user.id===ownProps.userId)};
    //...state.movies
    return state.voteState
};

export default connect(mapStateToProps) (PollResult);