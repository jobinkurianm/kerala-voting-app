import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button
} from 'react-native';

 
const Loading = ({props}) => (
    <View>
     <Text>Loading.....</Text>
    </View>
);
 
const styles = StyleSheet.create({
    flex1: {
        flex: 1
    }
});
 
export default Loading;