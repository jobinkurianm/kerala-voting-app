import React, { Component } from "react";
import config from '../config/web.config';
import { connect } from 'react-redux';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button
} from "react-native";
import { db } from '../config/db.config';
import * as voteActions from '../actions/voteActions'
import PollList from '../components/PollList';
import PollResult from '../components/PollResult';



class Poll extends Component {

    componentDidMount() {
        //this.props.fetchPosts();
        //  console.log(this.props.votes);
        this.props.loadVotes();
    }

    render() {
        return (
            <View style={styles.container}>
                    <View style={{
                        flex: 1,
                        width: 500,
                        height: 500,
                        alignItems: 'center',
                        alignContent: 'center',
                        justifyContent: 'space-around' 
                    }}>
                    {this.props.isShowResult ? <PollResult /> : <PollList />}
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    //console.log(state.voteState);
    return state.voteState
    /*return {
        votes: state.voteState.votes
        isLoading: state.voteState.isLoading,
        error: state.voteState.error
    }*/
}

function mapDispatchToProps(dispatch) {
//console.log(config);
    return {
        loadVotes: () => dispatch(voteActions.loadVote())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Poll)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});