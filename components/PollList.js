import React, { Component } from "react";
import config from '../config/web.config';
import { connect } from 'react-redux';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Button
} from "react-native";
import { db } from '../config/db.config';
import {surveyList} from '../config/surveyList';
import * as voteActions from '../actions/voteActions'
import ErrorPage from '../components/ErrorPage';
import Loading from '../components/Loading';
class PollList extends Component{

	render () {

        if (this.props.isLoading) {
            return (
                <Loading/>
            )
          }
          if (this.props.isError) {
            return (
                <ErrorPage/>
            )
          }

        return (
           <View> 
            <TouchableOpacity>    
            {surveyList.map(newVote => (<Button key={newVote.id} title={newVote.name} id= {newVote.id} onPress={() => this.props.castVote(newVote)}/>))}    
           </TouchableOpacity>
           </View>
        );
	}
}
function mapDispatchToProps(dispatch) {
    //console.log(config);
        return {
            castVote: newVote => dispatch(voteActions.castVote(newVote))
        }
    }
const mapStateToProps = (state, ownProps) => {
	//return {user: state.users.find(user => user.id===ownProps.userId)};
    //...state.movies
    return state.voteState
};

export default connect(mapStateToProps,mapDispatchToProps) (PollList);